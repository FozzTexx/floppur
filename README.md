# floppur: A raw flux encoder for floppy disks

## About

In here you will find a program I made that can take a raw sector
image and convert it into flux transitions that can be written out to
a physical floppy. The input files are ones compatible with most
emulators, such as .do for Apple, .d64 for Commodore, and .atr for
Atari. The output is SuperCard Pro format. The SuperCard Pro routines
[come from Mark Blair's
project](https://gitlab.com/NF6X_Retrocomputing/pySuperCardPro)
however I made some changes to them to add support for writing and to
move some things all into the same namespace to make it easier to
import, so to use this [you will need my
fork](https://gitlab.com/FozzTexx/pySuperCardPro).

This tool can do more though than just create a disk image from a
single file. It's also possible to put together a .json file which
specifies using multiple images as inputs and which sectors from those
images to use in the output. While so far I've only used this feature
to create a silly demo, I have a feeling it would be useful for more
than that.

Chris Osborn - [@FozzTexx](https://twitter.com/FozzTexx) - [insentricity.com](https://www.insentricity.com/a.cl/284/a-multi-format-cross-platform-floppy-for-525-floppy-day) - fozztexx@fozztexx.com

## Supported disk formats

* Apple II
* Commodore 64
* Atari 8 bit
* Victor 9000

## License

GPL 3
