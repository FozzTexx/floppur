##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

from collections import namedtuple

class SectorImage:
  SectorHeader = namedtuple("SectorHeader", ["id", "volume", "track", "sector",
                                             "checksum", "epilogue", "side", "length"])
  DataField = namedtuple("DataField", ["id", "data", "checksum", "epilogue"])
  EncodedSector = namedtuple("EncodedSector", ["gap0", "header_sync", "header",
                                               "gap1", "data_sync", "data_field",
                                               "gap2"])
  
  class Sector:
    def __init__(self, data):
      self.data = data
      return

    def encode(self, secImage, asTrackNum, asSectorNum):
      raise NotImplementedError("You must implement this in your subclass")

    def concatBits(self, enc_sector):
      track_bits = 0
      for val in enc_sector:
        if val is None:
          continue

        # v_len calc works because encoded bytes must begin with 1
        v_len = val.bit_length()
        track_bits <<= v_len
        track_bits |= val

      return track_bits
    
  class Track:
    def __init__(self, sectors):
      self.sectors = sectors
      return

    def encode(self, secImage, asTrackNum):
      track_bits = self.trackHeader(secImage, asTrackNum)
      for idx, sector in enumerate(self.sectors):
        sec_bits = sector.encode(secImage, asTrackNum, idx)
        v_len = sec_bits.bit_length()
        track_bits <<= v_len
        track_bits |= sec_bits
      return track_bits

    def trackHeader(self, secImage, asTrackNum):
      return 0

  @staticmethod
  def arrayToInt(val, bitlen=8):
    enc_val = 0
    for b in val:
      enc_val <<= bitlen
      enc_val |= b
    return enc_val

  def encodeTrack(self, trackNum):
    return self.tracks[trackNum].encode(self, trackNum)

  def encodeSector(self, trackNum, sectorNum):
    trk = self.tracks[trackNum]
    sec = trk.sectors[sectorNum]
    return sec.encode(self, trackNum, sectorNum)

  def trackHeader(self, trackNum):
    return self.tracks[trackNum].trackHeader(self, trackNum)
