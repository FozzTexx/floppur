##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

from fzfloppur.sectorimage import SectorImage
import crcmod
import struct

# https://atari.fox-1.nl/disk-formats-explained/

def encodeFM(val):
  fm_val = 0
  for b in val:
    for idx in range(8):
      fm_val <<= 2
      fm_val |= 2
      fm_val |= (b >> 7 - idx) & 1
  return fm_val

class AtariImage(SectorImage):
  class Sector(SectorImage.Sector):
    def encodeData(self, val):
      if isinstance(val, SectorImage.SectorHeader):
        fm_val = val.id
        bval = [val.track, val.side, val.sector, val.length,
                (val.checksum >> 8) & 0xff, val.checksum & 0xff]
        benc = encodeFM(bval)
        fm_val <<= benc.bit_length()
        fm_val |= benc
      elif isinstance(val, SectorImage.DataField):
        fm_val = val.id
        dval = encodeFM(val.data)
        dlen = (dval.bit_length() + 15) // 16
        fm_val <<= dlen * 16
        fm_val |= dval
        fm_val <<= 32
        enc_chk = encodeFM([(val.checksum >> 8) & 0xff, val.checksum & 0xff])
        fm_val |= enc_chk
      elif isinstance(val, (tuple, list, bytes, bytearray)):
        fm_val = encodeFM(val)
      else:
        fm_val = self.encodeData((val))
      return fm_val

    def encode(self, secImage, asTrackNum, asSectorNum):
      sec_num = asSectorNum + 1
      sec_len = 0x00

      # Taken from WD1771 datasheet - x^16 + x^12 + x^5 + 1
      polynomial = 1 << 16 | 1 << 12 | 1 << 5 | 1

      crc16 = crcmod.mkCrcFun(polynomial, rev=False)
      crc = crc16(bytes([secImage.sector_id, asTrackNum, 0x00, sec_num, sec_len]))
      header = SectorImage.SectorHeader(id=secImage.sector_mark, track=asTrackNum, side=0x00,
                                        sector=sec_num, length=sec_len,
                                        checksum=crc, volume=None, epilogue=None)
      header_enc = self.encodeData(header)

      inv_data = bytes([0xff ^ x for x in self.data])
      crc = crc16(bytes([secImage.data_id]) + inv_data)
      data_field = SectorImage.DataField(id=secImage.data_mark, data=inv_data,
                                         checksum=crc, epilogue=None)
      data_enc = self.encodeData(data_field)

      enc_sector = SectorImage.EncodedSector(header_sync=secImage.sync, header=header_enc,
                                             data_sync=secImage.sync, data_field=data_enc,
                                             gap0=secImage.gap0, gap1=secImage.gap1,
                                             gap2=secImage.gap2)
      return self.concatBits(enc_sector)
    
  class Track(SectorImage.Track):
    def trackHeader(self, secImage, asTrackNum):
      track_bits = encodeFM([0xff] * 10 + [0x00] * 6)
      idx_mark = encodeFM([0xfc])
      clock_d7 = ((encodeFM([0xd7]) << 1) | 1) & 0xffff
      idx_mark &= clock_d7
      track_bits <<= 16
      track_bits |= idx_mark
      return track_bits

  def __init__(self, path):
    self.tpi = 48
    self.rpm = 300
    self.sides = 1
    self.max_sectors = 18
    self.max_tracks = 40
    
    self.sync = encodeFM([0x00] * 6)
    self.gap0 = (1 << (16 * 12)) - 1
    self.gap1 = (1 << (16 * 11)) - 1
    self.gap2 = (1 << (16 * 10)) - 1

    clock_c7 = ((encodeFM([0xc7]) << 1) | 1) & 0xffff
    self.sector_id = 0xfe
    self.data_id = 0xfb
    self.sector_mark = encodeFM([self.sector_id]) & clock_c7
    self.data_mark = encodeFM([self.data_id]) & clock_c7

    with open(path, mode='rb') as file:
      self.data = file.read()

    values = struct.unpack("< HHHH B H 5s", self.data[:16])
    values_alt = struct.unpack("< HHHH I 3s B", self.data[:16])

    magic = values[0]
    size = (values[3] << 16) | values[1]
    self.sector_size = values[2]
    flags = values[4]
    protection = values[5]

    if magic != 0x0296:
      raise TypeError("Not an Atari disk")
    if size * 16 + 16 != len(self.data):
      raise ValueError("Image is corrupt")

    # FIXME - check values_alt to see if it's a valid CRC and if so, use that structure
    
    self.tracks = []
    sd = 0
    for tk in range(self.max_tracks):
      sectors = []
      for sc in range(self.max_sectors):
        sectors.append(self.Sector(self.dataForSector(sd, tk, sc)))
      self.tracks.append(self.Track(sectors))
      
    return
  
  def dataForSector(self, side, track, sector):
    offset = sector + self.max_sectors * track + self.max_sectors * self.max_tracks * side
    offset *= self.sector_size
    offset += 16
    if offset > len(self.data):
      return None
    return self.data[offset:offset+self.sector_size]

  @staticmethod
  def bitWidthForTrack(track):
    return 3500
