##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

from fzfloppur.sectorimage import SectorImage

apple_gcr = [0x96, 0x97, 0x9A, 0x9B, 0x9D, 0x9E, 0x9F, 0xA6,
             0xA7, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB2, 0xB3,
             0xB4, 0xB5, 0xB6, 0xB7, 0xB9, 0xBA, 0xBB, 0xBC,
             0xBD, 0xBE, 0xBF, 0xCB, 0xCD, 0xCE, 0xCF, 0xD3,
             0xD6, 0xD7, 0xD9, 0xDA, 0xDB, 0xDC, 0xDD, 0xDE,
             0xDF, 0xE5, 0xE6, 0xE7, 0xE9, 0xEA, 0xEB, 0xEC,
             0xED, 0xEE, 0xEF, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6,
             0xF7, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF]

apple_remap = [0x00, 0x0d, 0x0b, 0x09, 0x07, 0x05, 0x03, 0x01,
               0x0e, 0x0c, 0x0a, 0x08, 0x06, 0x04, 0x02, 0x0f]
apple_mapre = [0x00, 0x07, 0x0e, 0x06, 0x0d, 0x05, 0x0c, 0x04,
               0x0b, 0x03, 0x0a, 0x02, 0x09, 0x01, 0x08, 0x0f]

class AppleImage(SectorImage):
  class Sector(SectorImage.Sector):
    def oddEvenEncode(self, val):
      xx = 0
      for idx in range(0, 8, 2):
        xx <<= 2
        xx |= 2
        xx |= (val >> (7 - idx)) & 1
      for idx in range(1, 8, 2):
        xx <<= 2
        xx |= 2
        xx |= (val >> (7 - idx)) & 1
      return xx

    def encodeGCR(self, val):
      gcr_val = 0
      aux_buf = [0] * (342 - 256)
      for idx, b in enumerate(val):
        # ub = (b & 0b11111100) >> 2

        # gcr_val <<= 8
        # gcr_val |= e_val

        aux_idx = idx % 86
        aux_shift = (idx // 86) * 2
        bit_l = b & 1
        bit_u = b & 2
        aux_buf[aux_idx] |= bit_l << (aux_shift + 3)
        aux_buf[aux_idx] |= bit_u << (aux_shift + 1)

      all_buf = aux_buf + list(val)
      xor_buf = []
      for idx, b in enumerate(all_buf):
        pb = 0
        if idx:
          pb = all_buf[idx - 1]
        xor_buf.append(b ^ pb)
      xor_buf.append(all_buf[-1])

      for idx in range(len(xor_buf)):
        gcr_val <<= 8
        gcr_val |= apple_gcr[xor_buf[idx] >> 2]
      return gcr_val

    def encodeData(self, val):
      if isinstance(val, SectorImage.SectorHeader):
        gcr_val = val.id
        gcr_val <<= 16
        gcr_val |= self.oddEvenEncode(val.volume)
        gcr_val <<= 16
        gcr_val |= self.oddEvenEncode(val.track)
        gcr_val <<= 16
        gcr_val |= self.oddEvenEncode(val.sector)
        gcr_val <<= 16
        gcr_val |= self.oddEvenEncode(val.checksum)
        gcr_val <<= 24
        gcr_val |= val.epilogue
      elif isinstance(val, SectorImage.DataField):
        gcr_val = val.id
        dval = self.encodeData(val.data)
        dlen = (dval.bit_length() + 7) // 8
        gcr_val <<= dlen * 8
        gcr_val |= dval
        gcr_val <<= 24
        gcr_val |= val.epilogue
      elif isinstance(val, (tuple, list, bytes, bytearray)):
        return self.encodeGCR(val)
      else:
        gcr_val = self.encodeData((val))
      return gcr_val

    def encode(self, secImage, asTrackNum, asSectorNum):
      header = SectorImage.SectorHeader(id=0xD5AA96, volume=secImage.volume,
                                        track=asTrackNum, sector=asSectorNum,
                                        checksum=secImage.volume ^ asTrackNum ^ asSectorNum,
                                        epilogue=0xDEAAEB, side=None, length=None)
      header_enc = self.encodeData(header)
      data_field = SectorImage.DataField(id=0xD5AAAD, data=self.data,
                                         checksum=None, # Checksum is encoded into data
                                         epilogue=0xDEAAEB)
      data_enc = self.encodeData(data_field)

      enc_sector = SectorImage.EncodedSector(header_sync=None,
                                             header=header_enc,
                                             data_sync=secImage.data_sync, data_field=data_enc,
                                             gap0=None, gap1=None, gap2=secImage.sector_sync)
      return self.concatBits(enc_sector)

  class Track(SectorImage.Track):
    def trackHeader(self, secImage, asTrackNum):
      return secImage.header_sync

  def __init__(self, path):
    self.sector_size = 256
    self.tpi = 48
    self.rpm = 300
    self.sides = 1

    sync_byte = 0b1111111100
    self.header_sync = self.arrayToInt([sync_byte] * 10, 10)
    self.sector_sync = self.arrayToInt([sync_byte] * 20, 10)
    self.data_sync = self.arrayToInt([sync_byte] * 10, 10)
    self.volume = 254
    
    with open(path, mode='rb') as file:
      self.data = file.read()

    self.max_sectors = 16
    self.max_tracks = 35

    self.tracks = []
    for tk in range(self.max_tracks):
      sectors = []
      for sc in range(self.max_sectors):
        sectors.append(self.Sector(self.dataForSector(tk, sc)))
      self.tracks.append(self.Track(sectors))
      
    return
  
  def dataForSector(self, track, sector):
    offset = sector + self.max_sectors * track
    offset *= self.sector_size
    if offset > len(self.data):
      return None
    return self.data[offset:offset+self.sector_size]

  # Apple sector numbering is weird, remap it
  def encodeSector(self, trackNum, sectorNum):
    trk = self.tracks[trackNum]
    sec = trk.sectors[apple_mapre[sectorNum]]
    return sec.encode(self, trackNum, sectorNum)
  
  @staticmethod
  def bitWidthForTrack(track):
    bit_width = 4000 # nanoseconds
    # SCP seems to write slower, compensate
    return bit_width * (4 / 4.125)

