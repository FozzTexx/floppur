##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

class G64Image:
  def __init__(self, path):
    with open(path, mode='rb') as file:
      self.data = file.read()

    self.signature = self.data[:8]
    self.version = self.data[8]

    tk_count = self.data[9]
    tk_size = self.data[10] | self.data[11] << 8

    self.tracks = [None] * tk_count

    for idx in range(tk_count):
      n = 12 + 4 * idx
      offset = int().from_bytes(self.data[n:n+4], byteorder='little', signed=False)
      if not offset:
        continue

      dlen = int().from_bytes(self.data[offset:offset+2], byteorder='little', signed=False)

      n = 0x015c + 4 * idx
      speed = int().from_bytes(self.data[n:n+4], byteorder='little', signed=False)

      self.tracks[idx] = self.data[offset+2:offset+2+dlen]
    
    return
  
