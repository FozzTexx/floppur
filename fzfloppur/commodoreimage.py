##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

from fzfloppur.sectorimage import SectorImage

cbm_gcr = [0x0a, 0x0b, 0x12, 0x13, 0x0e, 0x0f, 0x16, 0x17,
           0x09, 0x19, 0x1a, 0x1b, 0x0d, 0x1d, 0x1e, 0x15]
zones = [[16, 21], [23, 19], [29, 18], [34, 17]]
bit_width_ns = [3268, 3612, 3813, 4037]

class CommodoreImage(SectorImage):
  class Sector(SectorImage.Sector):
    def encodeData(self, val):
      if isinstance(val, SectorImage.SectorHeader):
        ordered = [val.id, val.checksum, val.sector, val.track,
                   ord(val.volume[1]), ord(val.volume[0]), 0x0F, 0x0F]
        gcr_val = self.encodeData(ordered)
      elif isinstance(val, (tuple, list, bytes, bytearray)):
        gcr_val = 0
        for b in val:
          if b is not None:
            e_val = self.encodeData(b)
            # e_len calc works because no GCR value can be all zero
            e_len = (e_val.bit_length() + 9) // 10
            gcr_val <<= e_len * 10
            gcr_val |= e_val
      else:
        gcr_val = (cbm_gcr[val >> 4] << 5) | cbm_gcr[val & 0xf]
      return gcr_val

    # Not all encoded bytes will begin with a 1, make special concatBits
    def concatBits(self, enc_sector):
      track_bits = 0
      for val in enc_sector:
        if val is None:
          continue

        # v_len calc works because no GCR value can be all zero
        v_len = (val.bit_length() + 9) // 10
        track_bits <<= v_len * 10
        track_bits |= val

      return track_bits
    
    def encode(self, secImage, asTrackNum, asSectorNum):
      sector_sync = (1 << (10 * 4)) - 1
      data_sync = (1 << (10 * 4)) - 1
      gap1 = SectorImage.arrayToInt([0x55] * 11)
      gap2 = SectorImage.arrayToInt([0x55] * 4)

      volID = 'FZ'

      chk = asSectorNum ^ (asTrackNum + 1) ^ ord(volID[0]) ^ ord(volID[1])
      header = SectorImage.SectorHeader(id=0x08, track=asTrackNum+1, sector=asSectorNum,
                                        checksum=chk, volume=volID, epilogue=None,
                                        side=None, length=None)
      header_enc = self.encodeData(header)
      chk = 0
      for v in self.data:
        chk ^= v
      data_field = SectorImage.DataField(id=0x07, data=self.data,
                                         checksum=chk, epilogue=[0x00, 0x00])
      data_enc = self.encodeData(data_field)

      enc_sector = SectorImage.EncodedSector(header_sync=sector_sync, header=header_enc,
                                             data_sync=data_sync, data_field=data_enc,
                                             gap0=None, gap1=gap1, gap2=gap2)
      return self.concatBits(enc_sector)

  def __init__(self, path):
    self.sector_size = 256
    self.tpi = 48
    self.rpm = 300
    self.sides = 1
    
    with open(path, mode='rb') as file:
      self.data = file.read()

    self.max_sectors = zones[0][1]
    self.max_tracks = 0
    self.max_tracks = zones[-1][0] + 1

    sectors = []
    sector_count = 0
    for tk in range(self.max_tracks):
      sector_count += self.sectorCountForTrack(tk)
    sectors.append(sector_count)
      
    img_size = len(self.data)
    img_sectors = (img_size + self.sector_size - 1) // self.sector_size

    self.packed = False
    if img_sectors == sectors[0]:
      self.packed = True
      
    self.tracks = []
    for tk in range(self.max_tracks):
      sectors = []
      sec_count = self.sectorCountForTrack(tk)
      for sc in range(sec_count):
        sectors.append(self.Sector(self.dataForSector(tk, sc)))
      self.tracks.append(self.Track(sectors))
      
    return

  def sectorCountForTrack(self, track):
    for tk in zones:
      if tk is None:
        continue
      if track <= tk[0]:
        return tk[1]
    return None

  def dataForSector(self, track, sector):
    if not self.packed:
      offset = sector + self.max_sectors * track
    else:
      offset = sector
      for tk in range(track):
        offset += self.sectorCountForTrack(tk)
    offset *= self.sector_size
    if offset > len(self.data):
      return None
    return self.data[offset:offset+self.sector_size]

  def bitWidthForTrack(self, track):
    t_zone = self.zoneForTrack(track)
    return bit_width_ns[t_zone]

  @staticmethod
  def zoneForTrack(track):
    for idx, tk in enumerate(zones):
      if tk is None:
        continue
      if track <= tk[0]:
        return idx
    return None
