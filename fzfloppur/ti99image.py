##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

from sectorimage import SectorImage
import crcmod

# https://www.unige.ch/medecine/nouspikel/ti99/disks.htm

class TI99Image(SectorImage):
  class Track(SectorImage.Track):
    def encodeFM(self, val):
      fm_val = 0
      for b in val:
        for idx in range(8):
          fm_val <<= 2
          fm_val |= 2
          fm_val |= (b >> 7 - idx) & 1
      return fm_val
      
    def encodeData(self, val):
      if isinstance(val, SectorImage.SectorHeader):
        clock_c7 = (self.encodeFM(0xc7) << 1) & 0xffff
        fm_val = self.encodeFM(0xfe) & clock_c7
        fm_val <<= 16
        fm_val |= self.encodeFM(val.track)
        fm_val <<= 16
        fm_val |= self.encodeFM(val.side)
        fm_val <<= 16
        fm_val |= self.encodeFM(val.sector)
        fm_val <<= 16
        fm_val |= self.encodeFM(val.length)
        fm_val <<= 32
        fm_val |= self.encodeFM(val.checksum)
      elif isinstance(val, SectorImage.DataField):
        clock_c7 = (self.encodeFM(0xc7) << 1) & 0xffff
        fm_val = self.encodeFM(0xfb) & clock_c7
        fm_val <<= 16
        dval = self.encodeFM(val.data)
        dlen = (dval.bit_length() + 7) // 8
        fm_val <<= dlen * 8
        fm_val |= dval
        fm_val <<= 32
        fm_val |= self.encodeFM(val.checksum)
      elif isinstance(val, (tuple, list, bytes, bytearray)):
        return self.encodeFM(val)
      else:
        fm_val = self.encodeData((val))
      return fm_val

    def encode(self, trackNum):
      track_bits = 0
      header_sync = encodeFM([0x00] * 6)
      sector_sync = encodeFM([0x00] * 6)
      gap0 = (1 << (16 * 12)) - 1
      gap1 = (1 << (16 * 11)) - 1
      gap2 = (1 << (16 * 36)) - 1

      for idx in range(len(self.sectors)):
        sec = self.sectors[idx]
        sec_num = idx

        crc16 = crcmod.mkCrcFun(0x18005, rev=False, initCrc=0xFFFF, xorOut=0x0000)
        crc = crc16([trackNum, 0x00, sec_num, 0x01])
        header = SectorImage.SectorHeader(id=0xfe, track=trackNum, side=0x00,
                                          sector=sec_num, length=0x01,
                                          checksum=crc)
        header_enc = self.encodeData(header)
        crc = crc16(sec.data)
        data_field = SectorImage.DataField(id=0xfb, data=sec.data, checksum=crc)
        data_enc = self.encodeData(data_field)

        enc_sector = SectorImage.EncodedSector(header_sync=header_sync, header=header_enc,
                                               data_sync=data_sync, data_field=data_enc,
                                               gap0=None, gap1=None, gap2=sector_sync)
        header_sync = None

        for val in enc_sector:
          if val is None:
            continue
          
          # v_len calc works because all Apple II GCR bytes must begin with 1
          v_len = val.bit_length()
          track_bits <<= v_len
          track_bits |= val

      return track_bits

  def __init__(self, path):
    self.sector_size = 256
    self.tpi = 48
    self.rpm = 300
    self.sides = 1
    
    with open(path, mode='rb') as file:
      self.data = file.read()

    self.max_sectors = 16
    self.max_tracks = 35

    self.tracks = []
    for tk in range(self.max_tracks):
      sectors = []
      for sc in range(self.max_sectors):
        sectors.append(self.Sector(self.dataForSector(sd, tk, sc)))
      self.tracks.append(self.Track(sectors))
      
    return
  
  def dataForSector(self, side, track, sector):
    offset = sector + self.max_sectors * track + self.max_sectors * self.max_tracks * side
    offset *= self.sector_size
    if offset > len(self.data):
      return None
    return self.data[offset:offset+self.sector_size]

  @staticmethod
  def bitWidthForTrack(track):
    bit_width = 4000 # nanoseconds
    # SCP seems to write slower, compensate
    return bit_width * (4 / 4.125)
