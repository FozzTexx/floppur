##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

# precomp might be needed
# https://marc.info/?l=classiccmp&m=137609524004633

from fzfloppur.sectorimage import SectorImage

cbm_gcr = [0x0a, 0x0b, 0x12, 0x13, 0x0e, 0x0f, 0x16, 0x17,
           0x09, 0x19, 0x1a, 0x1b, 0x0d, 0x1d, 0x1e, 0x15]
zones = [[19, 3, 15, 26, 37, 47, 59, 70, 79, None],
         [18, None, 7, 18, 29, 39, 51, 62, 74, 79]]

## Values found by using --stretch
# bit_width_ns = [1533, 1615, 1712, 1821, 1942, 2081, 2241, 2428, 2623] # 360 RPM values

## Values found by averaging reads from original disks and newly formatted disks
bit_width_ns = [1498, 1583, 1675, 1775, 1897, 2031, 2182, 2384, 2620] # 360 RPM values

class VictorImage(SectorImage):
  class Sector(SectorImage.Sector):
    def encodeData(self, val):
      if isinstance(val, (tuple, list, bytes, bytearray)):
        gcr_val = 0
        for b in val:
          if b is not None:
            e_val = self.encodeData(b)
            # e_len calc works because no GCR value can be all zero
            e_len = (e_val.bit_length() + 9) // 10
            gcr_val <<= e_len * 10
            gcr_val |= e_val
      else:
        gcr_val = (cbm_gcr[val >> 4] << 5) | cbm_gcr[val & 0xf]
      return gcr_val

    # Not all encoded bytes will begin with a 1, make special concatBits
    def concatBits(self, enc_sector):
      track_bits = 0
      for val in enc_sector:
        if val is None:
          continue

        # v_len calc works because no GCR value can be all zero
        v_len = (val.bit_length() + 9) // 10
        track_bits <<= v_len * 10
        track_bits |= val

      return track_bits

    def encode(self, secImage, asTrackNum, asSectorNum):
      track_bits = 0
      sector_sync = (1 << (10 * 15)) - 1
      data_sync = (1 << (10 * 5)) - 1
      gap = self.encodeData([0x00] * 11)
      _, vTrack, vSide = secImage.physicalTrackToLogicalTrack(asTrackNum)
      asTrackNum = vSide * 0x80 + vTrack

      header = SectorImage.SectorHeader(id=0x07, track=asTrackNum, sector=asSectorNum,
                                        checksum=asTrackNum+asSectorNum,
                                        volume=None, epilogue=None,
                                        side=None, length=None)
      header_enc = self.encodeData(header)
      chk = sum(self.data) & 0xffff
      data_field = SectorImage.DataField(id=0x08, data=self.data,
                                         checksum=(chk & 0xff, chk >> 8), epilogue=None)
      data_enc = self.encodeData(data_field)

      enc_sector = SectorImage.EncodedSector(header_sync=sector_sync, header=header_enc,
                                             data_sync=data_sync, data_field=data_enc,
                                             gap0=gap, gap1=gap, gap2=gap)
      return self.concatBits(enc_sector)

  def __init__(self, path):
    self.sector_size = 512
    self.tpi = 96
    self.rpm = 360 # Drives are variable but the flux image will be for a 1.2MB 360rpm
    
    with open(path, mode='rb') as file:
      self.data = bytearray(file.read())

    self.max_sectors = max(zones[0][0], zones[1][0])
    self.max_tracks = self.tracksPerSide()
    sectors = self.sectorsPerSide()
    single_size = self.max_sectors * self.max_tracks

    img_size = len(self.data)
    img_sectors = (img_size + self.sector_size - 1) // self.sector_size

    self.packed = False
    self.sides = 1
    if img_sectors == sectors[0] or img_sectors == sectors[0] + 1:
      self.packed = True
    elif img_sectors == sectors[0] + sectors[1]:
      self.packed = True
      self.sides = 2
    elif img_sectors == single_size * 2:
      self.sides = 2
    else:
      raise ValueError("Unknown image structure. Invalid sector count:", img_sectors,
                       " - Should be", sectors[0], ",", sectors[0] + sectors[1],
                       ", or", single_size * 2)
      
    self.tracks = []
    for tk in range(self.max_tracks * self.sides):
      sectors = []
      for sc in range(self.sectorCountForTrack(tk)):
        sectors.append(self.Sector(self.dataForSector(tk, sc)))
      self.tracks.append(self.Track(sectors))

    return

  def encodeTrack(self, trackNum):
    vTrackNum, _, _ = self.physicalTrackToLogicalTrack(trackNum)
    track = self.tracks[vTrackNum]
    return track.encode(self, trackNum)

  def dataForSector(self, track, sector):
    if not self.packed:
      offset = sector + self.max_sectors * track + self.max_sectors * self.max_tracks * side
    else:
      offset = sector
      for tk in range(track):
        offset += self.sectorCountForTrack(tk)
    offset *= self.sector_size
    if offset > len(self.data):
      raise ValueError("Invalid sector", side, track, sector, offset,
                       self.sectorCountForTrack(side, track))
      return None
    return self.data[offset:offset+self.sector_size]

  def bitWidthForTrack(self, trackNum):
    vTrackNum, _, vSide = self.physicalTrackToLogicalTrack(trackNum)
    t_zone = VictorImage.zoneForTrack(vTrackNum)
    bit_width = bit_width_ns[t_zone]
    # SCP seems to write slower, compensate
    return bit_width * 0.97

  def physicalTrackToLogicalTrack(self, trackNum):
    # Victor disks have all tracks of one side followed by all tracks of the other
    # SCP format requires the two sides to be interlaced
    if self.sides == 1:
      return trackNum, trackNum, 0
    vTrack, vSide = divmod(trackNum, 2)
    vTrackNum = vTrack + vSide * self.max_tracks
    return vTrackNum, vTrack, vSide

  @staticmethod
  def createZoneTable():
    VictorImage.trackToZone = []
    offset = zone = 0
    last_track = 0
    for zside, z in enumerate(zones):
      last_track = 0
      sectors = z[0]
      for tk in z[1:]:
        if tk is None:
          continue
        for idx in range(last_track, tk + 1):
          VictorImage.trackToZone.append([zone, sectors, idx, zside])
        last_track = idx + 1
        sectors -= 1
        zone += 1
      zone = 1
    return

  @staticmethod
  def sectorCountForTrack(track):
    if not hasattr(VictorImage, "trackToZone"):
      VictorImage.createZoneTable()
    return VictorImage.trackToZone[track][1]

  @staticmethod
  def zoneForTrack(track):
    if not hasattr(VictorImage, "trackToZone"):
      VictorImage.createZoneTable()
    return VictorImage.trackToZone[track][0]

  @staticmethod
  def sectorsPerSide():
    sectors = []
    max_tracks = VictorImage.tracksPerSide()
    offset = 0
    for sd in range(len(zones)):
      sector_count = 0
      for tk in range(max_tracks):
        sector_count += VictorImage.sectorCountForTrack(tk + offset)
      offset += tk + 1
      sectors.append(sector_count)
    return sectors

  @staticmethod
  def tracksPerSide():
    if not hasattr(VictorImage, "trackToZone"):
      VictorImage.createZoneTable()
    return len(VictorImage.trackToZone) // 2
