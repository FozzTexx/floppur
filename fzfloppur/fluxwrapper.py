##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

import os, sys
import numpy as np

# This comes from nf6x's pySuperCardPro
# https://gitlab.com/NF6X_Retrocomputing/pySuperCardPro
from scp.scpfile import SCPfile

# This comes from brouhaha's fluxtoimd
# https://github.com/brouhaha/fluxtoimd
from fluxtoimd.kfsf import KFSF

class FluxWrapper:
  def __init__(self, path):
    _, ext = os.path.splitext(path)
    if ext == ".scp":
      self.scp_img = SCPfile(filename=path, debug=False)
    else:
      with open(path, 'rb') as f:
        self.kf_img = KFSF(f)
    return

  def fluxForTrack(self, tnum):
    if hasattr(self, 'scp_img'):
      flux = self.scp_img.tracks[tnum].fluxdata
      # FIXME - merge zero cells
    else:
      period_360 = (60 * 1000000000) / 360
      n_trk = np.array(self.kf_img.blocks[(tnum, 0, 1)].flux_trans_abs)
      rev_limit = period_360 / self.fluxResolution
      indexes = self.kf_img.blocks[(0, 0, 1)].index_abs
      #print("INDEXES", indexes)
      rel_indexes = [(indexes[i] - indexes[i-1]) * self.fluxResolution
                     for i in range(1, len(indexes))]
      #print("RELATIVE", rel_indexes)
      use_index = 1
      idx_range = np.where(np.logical_and(n_trk >= indexes[use_index],
                                          n_trk <= indexes[use_index + 1]))
      #print("MATCH", idx_range[0][0], idx_range[0][-1], n_trk[idx_range[0][0]])
      rel_trk = n_trk[idx_range]
      # print("CELLS", rel_trk[0], rel_trk[-1],
      #       (rel_trk[-1] - rel_trk[0]) * self.fluxResolution,
      #       period_360 - (rel_trk[-1] - rel_trk[0]) * self.fluxResolution)
      flux = [rel_trk[i] - rel_trk[i-1] for i in range(1, len(rel_trk))]
      flux.insert(0, rel_trk[0] - indexes[use_index])
      #print("FLUX", len(flux), len(idx_range[0]))
    return [x * self.fluxResolution for x in flux]

  def findSync(self, tnum, after):
    return

  @property
  def fluxResolution(self):
    if hasattr(self, 'scp_img'):
      return 25;
    return 1000000000 / self.kf_img.blocks[(0, 0, 1)].frequency
  
