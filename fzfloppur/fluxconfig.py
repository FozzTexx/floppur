##########################################################################
# Copyright 2021 by Chris Osborn <fozztexx@fozztexx.com>
#
# This file is part of floppur.
#
#  floppur is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.
#
#  floppur is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with floppur.  If not, see <http://www.gnu.org/licenses/>.
##########################################################################

import sys
from fzfloppur.victorimage import VictorImage
from fzfloppur.appleimage import AppleImage
from fzfloppur.commodoreimage import CommodoreImage
from fzfloppur.atariimage import AtariImage

FLUX_RESOLUTION = 25 # nanoseconds
PERIOD_360 = (60 * 1000000000) / 360
PERIOD_300 = (60 * 1000000000) / 300

def reverse_bits(num):
  result = 0
  while num:
    result = (result << 1) + (num & 1)
    num >>= 1
  return result

class FluxConfig:
  def __init__(self, config):
    self.sources = config['images']
    self.output = None
    if 'tracks' in config:
      self.output = config['tracks']

    for key in self.sources:
      src = self.sources[key]
      # Dirty sneaky runtime stuff to convert the type string into a
      # class instead of making a really long if/elif statement
      image_class = src['type'].capitalize() + "Image"
      img = getattr(sys.modules[__name__], image_class)(src['file'])
      src['image'] = img

    if self.output is None:
      name = list(self.sources.keys())[0]
      src = self.sources[name]
      img = src['image']
      self.output = {}
      for trk_idx in range(self.highestTrack()):
        self.output[trk_idx] = [[name, trk_idx]]
    else:
      i_output = {}
      for key in self.output:
        sectors = self.output[key]
        expanded_sectors = []
        for sec in sectors:
          if len(sec) > 2 and isinstance(sec[2], (tuple, list)):
            for n in sec[2]:
              expanded_sectors.append([sec[0], sec[1], n])
          else:
            expanded_sectors.append(sec)  
        i_output[int(key)] = expanded_sectors
      self.output = i_output
    return

  def highestTrack(self):
    highest = None
    if self.output:
      highest = max(self.output.keys()) + 1
    else:
      for key in self.sources:
        src = self.sources[key]
        max_tracks = src['image'].max_tracks * src['image'].sides
        if highest is None or max_tracks > highest:
          highest = max_tracks
    return highest

  def stretch(self, t_data, period, max_bits, cell_ns):
    # Stretch cells so that gap at end fits in one cell
    unused_bits = int(max_bits) - t_data.bit_length()
    if unused_bits < 0:
      cell_ns = period / t_data.bit_length()
    else:
      unused_time = unused_bits * cell_ns
      unused_flux = int(unused_time / FLUX_RESOLUTION)
      gap = min(unused_flux, 65535)
      gap_ns = gap * FLUX_RESOLUTION
      cell_ns = (period - gap_ns) / t_data.bit_length()

    #print("CELL", idx, cell_ns, int(cell_ns * 1.2), bit_width)
    return cell_ns

  def bitsToFlux(self, t_data, cell_ns, period):
    flux_len = 1
    flux_data = []
    t_rev = reverse_bits(t_data)
    while t_rev:
      flux_bit = t_rev & 1
      t_rev >>= 1
      if flux_bit:
        flux_time = cell_ns * flux_len
        # if flux_time <= cell_ns * 1.2:
        #   flux_time *= 0.95
        flux_data.append(int(flux_time / FLUX_RESOLUTION))
        flux_len = 0
      flux_len += 1
    rev_remain = period - sum(flux_data) * FLUX_RESOLUTION
    if rev_remain < 0:
      print("\nData doesn't fit on track", rev_remain, t_data.bit_length(),
            file=sys.stderr)
      new_cell_ns = cell_ns * period / (sum(flux_data) * FLUX_RESOLUTION)
      print("Fudging cell_ns", cell_ns, "==", new_cell_ns)
      return self.bitsToFlux(t_data, new_cell_ns, period)
    return flux_data
    
  def convertToFlux(self, rpm, stretch=False):
    period = PERIOD_300
    if rpm == 360:
      period = PERIOD_360
    total_tracks = self.highestTrack()

    flux_tracks = [None] * total_tracks

    for idx in self.output:
      print("Track: %i" % (idx), end='\r')
      sectors = self.output[idx]
      used_headers = []
      for sec in sectors:
        img = self.sources[sec[0]]['image']
        tnum = sec[1]
        if len(sec) == 2:
          t_data = img.encodeTrack(tnum)
          if sec[0] not in used_headers:
            used_headers.append(sec[0])
        else:
          t_data = img.encodeSector(tnum, sec[2])
          if sec[0] not in used_headers:
            used_headers.append(sec[0])
            t_header = img.trackHeader(tnum)
            t_header <<= t_data.bit_length()
            t_data |= t_header
          
        bit_width = img.bitWidthForTrack(idx)
        max_bits = period / bit_width
        #cell_ns = PERIOD_360 / max_bits
        cell_ns = bit_width

        if stretch:
          # FIXME - only do this for entire tracks - ie if len(sec) == 2 above
          cell_ns = self.stretch(t_data, period, max_bits, cell_ns)
          print("Stretching", period, max_bits, t_data.bit_length(), cell_ns, bit_width)

        flux_data = self.bitsToFlux(t_data, cell_ns, period)

        if flux_tracks[idx] is None:
          flux_tracks[idx] = []
        flux_tracks[idx].extend(flux_data)

      rev_remain = period - sum(flux_tracks[idx]) * FLUX_RESOLUTION
      gap_ns = int(rev_remain / FLUX_RESOLUTION)
      while gap_ns > 65535:
        flux_tracks[idx].append(0)
        gap_ns -= 65536
      flux_tracks[idx].append(gap_ns)
        
    return flux_tracks

  @property
  def rpm(self):
    name = list(self.sources.keys())[0]
    src = self.sources[name]
    img = src['image']
    return img.rpm

  @property
  def tpi(self):
    name = list(self.sources.keys())[0]
    src = self.sources[name]
    img = src['image']
    return img.tpi

  @property
  def sides(self):
    name = list(self.sources.keys())[0]
    src = self.sources[name]
    img = src['image']
    return img.sides
